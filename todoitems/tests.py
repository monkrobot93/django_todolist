from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from .models import ToDo


# class ToDoModelTest(TestCase):
# 
#     def setUp(self):
#         ToDo.objects.create(title='test1', body='body1')
# 
#     def test_content(self):
#         todo = ToDo.objects.get(id=1)
#         expected_object_title = f'{todo.title}'
#         self.assertEqual(expected_object_title, 'test1')
#         expected_object_body = f'{todo.body}'
#         self.assertEqual(expected_object_body, 'body1')
# 
# 
# class HomePageViewTest(TestCase):
# 
#     def setUp(self):
#         ToDo.objects.create(title='test1', body='body1')
# 
#     def test_view_url_exists(self):
#         response = self.client.get('/')
#         self.assertEqual(response.status_code, 200)
# 
#     def test_view_url_by_name(self):
#         response = self.client.get(reverse('home'))
#         self.assertEqual(response.status_code, 200)
# 
#     def test_view_uses_correct_template(self):
#         response = self.client.get(reverse('home'))
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'home.html')

    
class ToDoTests(TestCase):

    def setUp(self):

        self.user = get_user_model().objects.create_user(
            username='testuser',
            email='testuser@ex.com',
            password='secret',
        )
        self.todo = ToDo.objects.create(
            title = 'good',
            body = 'todo_body',
            author = self.user,
        )

    def test_string_representation(self):
        todo = ToDo(title='good')
        self.assertEqual(str(todo), todo.title)

    def test_get_absolute_url(self):
        self.assertEqual(self.todo.get_absolute_url(), '/todo/1/')

    def test_todo_content(self):
        self.assertEqual(f'{self.todo.title}', 'good')
        self.assertEqual(f'{self.todo.body}', 'todo_body')
        self.assertEqual(f'{self.todo.author}', 'testuser')

    def test_todo_list_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'todo_body')
        self.assertTemplateUsed(response, 'home.html')

    def test_todo_detail_view(self):
        response = self.client.get('/todo/1/')
        no_response = self.client.get('/todo/1000/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'todo_body')
        self.assertTemplateUsed(response, 'todo_detail.html')

    def test_todo_create_view(self):
        response = self.client.post(reverse('todo_new'), {
            'title': 'New title',
            'body': 'New test',
            'author': self.user,
        })
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'New title')
        self.assertContains(response, 'New test')

    def test_todo_update_view(self):
        response = self.client.post(reverse('todo_edit', args='1'),{
            'title': 'Updated title',
            'body': 'Updated test',
        })
        self.assertEqual(response.status_code, 302)

    def test_todo_delete_view(self):
        response = self.client.get(reverse('todo_delete', args='1'))
        self.assertEqual(response.status_code, 200)
