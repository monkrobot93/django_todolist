from django.urls import path
from .views import HomePageView, ToDoDetailView, ToDoCreateView, ToDoUpdateView, ToDoDeleteView


urlpatterns = [
    path('todo/<int:pk>/delete/', ToDoDeleteView.as_view(), name='todo_delete'),
    path('', HomePageView.as_view(), name='home'),
    path('todo/<int:pk>/', ToDoDetailView.as_view(), name='todo_detail'),
    path('todo/new/', ToDoCreateView.as_view(), name='todo_new'),
    path('todo/<int:pk>/edit/', ToDoUpdateView.as_view(), name='todo_edit'),
]