from django.db import models
from django.contrib.auth import get_user_model
from django.urls import reverse


class ToDo(models.Model):
    DONE = "Done"
    IN_PROC = "In work"
    STATE = [
        (DONE, "done"),
        (IN_PROC, "In work")
        ]
    state = models.CharField(
        max_length=7,
        choices=STATE,
        default=IN_PROC,
    )
    title = models.CharField(max_length=255)
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('todo_detail', args=[str(self.id)])
