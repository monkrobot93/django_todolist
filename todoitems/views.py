from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .models import ToDo


class HomePageView(ListView):
    model = ToDo
    template_name = 'home.html'


class ToDoDetailView(DetailView):
    model = ToDo
    template_name = 'todo_detail.html'


class ToDoCreateView(CreateView):
    model = ToDo
    template_name = 'todo_new.html'
    fields = ['title', 'body', 'author', 'state']


class ToDoUpdateView(UpdateView):
    model = ToDo
    template_name = 'todo_edit.html'
    fields = ['title', 'body', 'state']


class ToDoDeleteView(DeleteView):
    model = ToDo
    template_name = 'todo_delete.html'
    success_url = reverse_lazy('home')
